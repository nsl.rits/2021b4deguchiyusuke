#ifndef _zrp_h_
#define _zrp_h_

/* [SECTION-1]------------------------------------------------------------------
 * INIT SECTION:- HEADERS AND PRE-PROCESSOR MACROS.
 * -----------------------------------------------------------------------------
 */

#include <config.h>
#include <assert.h>
#include <agent.h>
#include <packet.h>
#include <ip.h>
#include <delay.h>
#include <scheduler.h>
#include <queue.h>
#include <trace.h>
#include <arp.h>
#include <ll.h>
#include <mac.h>
#include <priqueue.h>
#include <delay.h>

#include <random.h>
#include <object.h>
#include <route.h>
#include <cmu-trace.h>

#include "constants.h"

#if defined(WIN32) && !defined(snprintf)
#define snprintf _snprintf
#endif /* WIN32 && !snprintf */

#if !defined(NULL)	/* Defining NULL = 0 */
#define NULL 0
#endif

int numnodes = 0;
int dnsty;
int flag = 0;
int old_d;
int new_d;
double sum;
const double c=0.4;
const double pi=3.14;
double ep;
double ev;
int a=3;
int nump;
int ev_array[5];
int max;
int ap;
int max_radius = 5;

/* [SECTION-2]------------------------------------------------------------------
 * NDP (NEIGHBOR DISCOVERY PROTOCOL):- SUBSECTIONS CONTAINS FOLLOWING THINGS:
 *	1) NEIGHBOR-LIST 		(Neighbor Table)
 *	2) BEACON-TRANSMIT TIMER	(Periodic Beacon Xmission)
 *	3) ACK-TIMEOUT TIMER		(ACK-Timeout Checking)
 *	4) NDP AGENT			(Neighbor Discovery Agent)
 * -----------------------------------------------------------------------------
 * [SUB-SECTION-2.1]------------------------------------------------------------
 * NEIGHBOR-LIST:- NEIGHBOR TABLE AND ASSOCIATED FUNCTIONS
 * -----------------------------------------------------------------------------
 */
/*!	\class Neighbor
	\brief This class contains NEIGHBOR NODE information.
 */
class Neighbor
{
  public:		
	nsaddr_t addr_;		// 32 bit Address
  	Time lastack_;		// Last ACK received Time
	int linkStatus_;	// Link-Status with this Neighbor
	int AckTOCount_;	// No-of-times ACK-TO has occured with this Nb
	Neighbor *next_;	// Pointer to Next Element

	// Constructors...
	Neighbor() : addr_(-1), lastack_(-1), linkStatus_(LINKDOWN),
		next_(NULL) {}	// Initialize with Invalid Entries
	Neighbor(nsaddr_t addr, Time lastack, int linkStatus) : addr_(addr),
		lastack_(lastack), linkStatus_(linkStatus), AckTOCount_(0),
		next_(NULL) {} 
};

/*!	\class NeighborList
	\brief This class is the linked-list of Neighbor class.
 */
class NeighborList
{
  public:
	Neighbor *head_;	// Pointer to head
	int numNeighbors_;	// Number of Neighbors

	// Constructors...
	NeighborList() : head_(NULL), numNeighbors_(0) {}

	// Methods...
	void addNeighbor(nsaddr_t addr, Time lastack, int linkStatus);
	int findNeighbor(nsaddr_t addr, Neighbor **handle);
	int isEmpty();
	void removeNeighbor(Neighbor *prev, Neighbor *toBeDeleted);
	void purgeDownNeighbors();
	void freeList();
	void printNeighbors();	
};

class ZRPAgent;		// Pre-Declaration... [Needed for Timers]
/* [SUB-SECTION-2.2]------------------------------------------------------------
 * BEACON-TRANSMIT TIMER:- PERIODIC BEACON XMISSION
 * -----------------------------------------------------------------------------
 */
/*!	\class NDPBeaconTransmitTimer
	\brief This class implements a BEACON Transmit Timer, who periodically sends beacons to NEIGHBOR nodes.
 */
class NDPBeaconTransmitTimer : public Handler 
{
  public:
	// Data...
	ZRPAgent *agent_;		// Pointer to ZRP-Agent
	Event intr_;		
	int beacon_period_; 		// Inter-Beacon-Time
	int beacon_period_jitter_;	// Jitter added to Inter-Beacon-Time
	int neighbor_ack_timeout_; 	// Only for Logging

	// Constructor...
	NDPBeaconTransmitTimer(ZRPAgent* agent) : agent_(agent),
		beacon_period_(DEFAULT_BEACON_PERIOD), 
		beacon_period_jitter_(DEFAULT_BEACON_PERIOD_JITTER),
		neighbor_ack_timeout_(DEFAULT_NEIGHBOR_ACK_TIMEOUT) {}

	void handle(Event*);  		// function handling the event
	void start(double thistime);
};

/* [SUB-SECTION-2.3]------------------------------------------------------------
 * ACK-TIMEOUT TIMER:- ACK-TIMEOUT CHECKING
 * -----------------------------------------------------------------------------
 */
/*!	\class NDPAckTimer
	\brief This class implements a Acknoledgement TimeOut Timer, who checks
	       that ACK is received or not for sent beacon.
 */
class NDPAckTimer : public Handler 
{
  public:
	ZRPAgent *agent_;		// Pointer to ZRP-Agent
	Event intr_;
	int neighbor_ack_timeout_;	// Time before a neighbor must send ACK	

	// Constructor...
	NDPAckTimer(ZRPAgent* agent) : agent_(agent),
		neighbor_ack_timeout_(DEFAULT_NEIGHBOR_ACK_TIMEOUT) {}

  	void handle(Event*);  		// function handling the event
	void start();
};

/* [SUB-SECTION-2.4]------------------------------------------------------------
 * NDP AGENT:- NEIGHBOR DISCOVERY AGENT
 * -----------------------------------------------------------------------------
 */
/*!	\class NDPAgent
	\brief This class implements the Neighbor Discovery Protocol (NDP).
 */
class NDPAgent
{
  public:
	// Data...
	ZRPAgent *agent_;		// Pointer to ZRP-Agent
	NeighborList neighborLst_;	// Neighbor List
	NDPBeaconTransmitTimer BeaconTransmitTimer_;	// Beacon-Xmit-Timer
	NDPAckTimer AckTimer_;				// ACK-TO-Timer
	int startup_jitter_;		// For starting the Timers

	// Constructors...
	/*!	\fn NDPAgent(ZRPAgent *)

		This constructor is called by the constructor of ZRP. It initializes 
		different compenents in NDP.
	 */
	NDPAgent(ZRPAgent *agent) : agent_(agent), BeaconTransmitTimer_(agent),
		AckTimer_(agent), startup_jitter_(DEFAULT_STARTUP_JITTER) {}
	
	// Methods...
	/*!	\fn void startUp()
		
		This is called by ZRPAgent::startUp() method. It does following tasks: \n
		- Starts the Beacon-Transmit-Timer. \n
		- Clears the Neighbor-List.
	*/
	void startUp();
	/*!	\fn void recv_NDP_BEACON(Packet* p)
		
		This function is called whenever ZRP receives an NDP_BEACON packet. On receving 
		'NDP_BEACON', node sends 'NDP_BEACON_ACK' to the sender. But it does not add this 
		neighbor, because node stores only symmetric links.
	*/
	void recv_NDP_BEACON(Packet* p);
	/*!	\fn void recv_NDP_BEACON_ACK(Packet* p)
		
		This function is called whenever ZRP receives an NDP_BEACON_ACK packet. \n
		- If it's a new neighbor, node adds the neighbor to its 'NEIGHBOR_TABLE' and 
		notifies IARP to rebuild the 'INNER_ROUTING_TABLE' and 'PERIPHERAL_NODE_TABLE'. \n
		- If it's an existing neighbor, it updates 'LAST_ACK_TIME' for that neighbor.
	*/
	void recv_NDP_BEACON_ACK(Packet* p);
	/*!	\fn void print_tables()
		
		This function prints the Neighbor-List. It also prints which Neighbors are UP 
		and which are DOWN.
	*/
	void print_tables();
};





/* [SECTION-3]------------------------------------------------------------------
 * IARP (INTRA-ZONE ROUTING PROTOCOL):- SUBSECTIONS CONTAINS FOLLOWING THINGS:
 *	1) LINKSTATE LIST 		(Topology Table)
 *	2) PERIPHERAL-NODE LIST		(Peripheral-Node Table)
 *	3) INNER-ROUTE LIST		(Inner-Route Table)
 *	4) UPDATE-DETECT LIST		(Update-detect Cache)
 *	5) PERIODIC-UPDATE TIMER	(Periodic Update Xmission)
 *	6) EXPIRATION-CHECK TIMER	(For Expiration of LinkState and Routes)
 *	7) IARP AGENT			(IntrA-Zone Routing Agent)
 * -----------------------------------------------------------------------------
 * [SUB-SECTION-3.1]------------------------------------------------------------
 * LINKSTATE LIST:- TOPOLOGY TABLE
 * -----------------------------------------------------------------------------
 */
class LinkState
{
  public:
	// Data...
  	nsaddr_t src_;  	// 32 bits
  	nsaddr_t dest_;  	// 32 bits
  	int seq_;  		// sequence number
  	int isup_;		// Link State LINKUP[1]/LINKDOWN[0]
  	Time expiry_;		// Link Expiry Time
	LinkState *next_;	// Pointer to Next Element

	// Constructors...
	LinkState() : src_(-1), dest_(-1), seq_(-1),
		isup_(-1), expiry_(-1.0), next_(NULL) {}
	LinkState(nsaddr_t src, nsaddr_t dest, int seq, int isup, Time expiry) {
		src_ = src;
	  	dest_ = dest;
  		seq_ = seq;
  		isup_ = isup;
  		expiry_ = expiry;
		next_ = NULL;
	}
};
class LinkStateList
{
  public:
	// Data...
	LinkState *head_;
	int numLinks_;

	// Constructors...
	LinkStateList() : head_(NULL), numLinks_(0) {}
	
	// Methods...
	void addLink(nsaddr_t src, nsaddr_t dest, int seq, int isup, Time expiry);
	int findLink(nsaddr_t src, nsaddr_t dest, LinkState **handle);
	int isEmpty();
	void removeLink(LinkState *prev, LinkState *toBeDeleted);
	int purgeLinks();
	void printLinks();
	void freeList();
};

/* [SUB-SECTION-3.2]------------------------------------------------------------
 * PERIPHERAL-NODE LIST:- PERIPHERAL-NODE TABLE
 * -----------------------------------------------------------------------------
 */
class PeripheralNode
{
  public:
	// Data...
	nsaddr_t addr_;		// Address of the Node
	int coveredFlag_;	// Only For IERP
	PeripheralNode *next_;	// Pointer to the next element

	// Constructor...	
	PeripheralNode() : addr_(-1), coveredFlag_(FALSE), next_(NULL) {}
	PeripheralNode(nsaddr_t addr, int coveredFlag) : addr_(addr),
		coveredFlag_(coveredFlag), next_(NULL) {}
};
class PeripheralNodeList
{
  public:
	// Data...
	PeripheralNode *head_;
	int numPerNodes_;

	// Constructor...	
	PeripheralNodeList() : head_(NULL), numPerNodes_(0) {}

	// Methods...
	void addPerNode(nsaddr_t addr, int coveredFlag);
	int findPerNode(nsaddr_t addr);
	void copyList(PeripheralNodeList *newList);
	void freeList();
};

/* [SUB-SECTION-3.3]------------------------------------------------------------
 * INNER-ROUTE LIST:- INNER-ROUTE TABLE
 * -----------------------------------------------------------------------------
 */
class InnerRoute
{
  public:
	// Data...
	nsaddr_t addr_;		// Destination Address
	nsaddr_t nextHop_;	// Next hop to reach that destination
	int numHops_;		// # of hops to the Destination
	InnerRoute *next_;	// Next entry in the Routelist
	InnerRoute *predecessor_; // To construct bordercast tree
	
	// Constructors...
	InnerRoute() : addr_(-1), nextHop_(-1), numHops_(-1), next_(NULL),
		predecessor_(NULL) {}
	InnerRoute(nsaddr_t addr, nsaddr_t nextHop, InnerRoute *predecessor, int numHops) {
		addr_ = addr;
		nextHop_ = nextHop;
		predecessor_ = predecessor;
		numHops_ = numHops;
		next_ = NULL;
	}
};
class InnerRouteList
{
  private:	
	InnerRoute *tail_;	// Used in addRoute()... 
  public:	
	// Data...
	InnerRoute *head_;
	int numRoutes_;
	
	// Constructor...
	InnerRouteList() : tail_(NULL), head_(NULL), numRoutes_(0) {}
	
	// Methods...
	void addRoute(nsaddr_t addr, nsaddr_t nextHop, InnerRoute *predecessor,
		int numHops);
	int findRoute(nsaddr_t addr, InnerRoute **handle);
	void freeList();
};

/* [SUB-SECTION-3.4]------------------------------------------------------------
 * UPDATE-DETECT LIST:- UPDATE-DETECT CACHE
 * -----------------------------------------------------------------------------
 */
class IARPUpdate
{
  public:
	// Data...
	nsaddr_t updateSrc_;	// Address of the sender
	int seq_;		// Sequence Number
  	Time expiry_;		// Expiry Time	
	IARPUpdate *next_;	// Pointer to the Next Element

	// Constructor...
	IARPUpdate() : 	updateSrc_(-1), seq_(-1), expiry_(-1), next_(NULL) {}
	IARPUpdate(nsaddr_t updateSrc, int seq, Time expiry) {
		updateSrc_ = updateSrc;
		seq_ = seq;
		expiry_ = expiry;
		next_ = NULL;
	}
};
class IARPUpdateDetectedList
{
  public:
	// Data...
	IARPUpdate *head_;
	int numUpdates_;
	
	// Constructor...
	IARPUpdateDetectedList() : head_(NULL), numUpdates_(0) {}
	
	// Methods...
	void addUpdate(nsaddr_t updateSrc, int seq, Time expiry);
	int findUpdate(nsaddr_t updateSrc, int seq);
	void purgeExpiredUpdates();
	void freeList();
};

/* [SUB-SECTION-3.5]------------------------------------------------------------
 * PERIODIC-UPDATE TIMER:- PERIODIC UPDATE XMISSION
 * -----------------------------------------------------------------------------
 */
class IARPPeriodicUpdateTimer : public Handler
{
 public:
	// Data...
	ZRPAgent *agent_;		// Pointer to ZRP-Agent
	Event intr_;	
	int min_iarp_update_period_;	// Min-Update Interval
	int max_iarp_update_period_;	// Max-Update Interval
	Time lastUpdateSent_;		// Last-Update Sent Time

	// Constructor...
	IARPPeriodicUpdateTimer(ZRPAgent* agent) : agent_(agent),
		min_iarp_update_period_(DEFAULT_MIN_IARP_UPDATE_PERIOD),
		max_iarp_update_period_(DEFAULT_MAX_IARP_UPDATE_PERIOD),
		lastUpdateSent_(0.0) {}
	// Methods...
  	void handle(Event*);  		// function handling the event
  	void start(double thistime);
	
};

/* [SUB-SECTION-3.6]------------------------------------------------------------
 * EXPIRATION-CHECK TIMER:- FOR EXPIRATION OF LINKSTATE AND ROUTES
 * -----------------------------------------------------------------------------
 */
class IARPExpirationTimer : public Handler
{
  public:	
	// Data...
	ZRPAgent *agent_;		// Pointer to ZRP-Agent
	Event intr_;
	int expiration_check_period_;	// Expiration-Check-Interval
	
	// Constructor...
	IARPExpirationTimer(ZRPAgent* agent) : agent_(agent),
		expiration_check_period_(DEFAULT_EXPIRATION_CHECK_PERIOD) {}

	// Methods...
  	void handle(Event*);  		// function handling the event
  	void start(double thistime);
};

/* [SUB-SECTION-3.7]------------------------------------------------------------
 * IARP AGENT:- INTRA-ZONE ROUTING AGENT
 * -----------------------------------------------------------------------------
 */
/*!	\class IARPAgent
	\brief This class implements the IntrAzone Routing Protocol (IARP).
 */
class IARPAgent
{
  public:
	// Data...
	ZRPAgent *agent_;	// Pointer to ZRP-Agent
	int updateSendFlag_;	// Intialized with TRUE [periodic update is sent
				// if this flag is TRUE; After sending each update,
				// this flag is set FALSE]
	// Lists...
	LinkStateList lsLst_;	// Topology Table
	PeripheralNodeList pnLst_;	// Peripheral-Node Table
	InnerRouteList irLst_;	// Proactive-Routing Table
	IARPUpdateDetectedList upLst_;	// Detected-Update Cache
	int linkLifeTime_;	// DEFAULT_LINK_LIFETIME
	int updateLifeTime_;	// DEFAULT_UPDATE_LIFETIME

	// Timers...
  	IARPPeriodicUpdateTimer PeriodicUpdateTimer_;
	IARPExpirationTimer ExpirationTimer_; 
	int startup_jitter_;	// For starting the Timers
	
	// Constructors...
	/*!	\fn IARPAgent(ZRPAgent *)

		This constructor is called by the constructor of ZRP. It initializes 
		different compenents in IARP.
	 */
	IARPAgent(ZRPAgent *agent) : agent_(agent), updateSendFlag_(TRUE),
		linkLifeTime_(DEFAULT_LINK_LIFETIME),
		updateLifeTime_(DEFAULT_UPDATE_LIFETIME), 
		PeriodicUpdateTimer_(agent), ExpirationTimer_(agent),
		startup_jitter_(DEFAULT_STARTUP_JITTER) {}

	// Methods...
	/*!	\fn void startUp()
		
		This is called by ZRPAgent::startUp() method. It does following tasks: \n
		- Starts the Periodic-Update-Timer & Expiration-Timer. \n
		- Clears the LinkState-List, PeripheralNode-List, Inner-Route-List & 
		Detected-Update-List.
	*/
	void startUp();		 // Starting Timers & Clearing Lists(Tables)
	/*!	\fn void buildRoutingTable()
		
		This function creates Inner-Route-List & Peripheral-Node-List based on 
		Link-State-List. The following steps are performed.\n
		-# Clear the existing Inner-Route-List & Peirpheral-Node-List.
		-# Purge the expired links & DOWN links.
		-# Generate a BFS tree from existing Link-State list.
		-# Generate Inner-Routing-List based on that BFS tree. Insert all leaf 
		   nodes in Peripheral-Node-List.
	*/
	void buildRoutingTable();// Create Routing-Table based on Topology-Table
	/*!	\fn void addRouteInPacket(nsaddr_t dest, Packet *p)
		
		This function adds the Inner-route for node 'dest' in packet 'p' if available.
	*/
	void addRouteInPacket(nsaddr_t dest, Packet *p);
	/*!	\fn void recv_IARP_UPDATE(Packet* p)
		
		This function is called whenever ZRP receives an IARP_UPDATE packet. On 
		receiving IARP_UPDATE, following tasks are performed -
		- If it's a new iarp_update then,
			- node updates its 'LINK_STATE_TABLE',
			- If 'LINK_STATE_TABLE' is changed then, it rebuilds the 
			'INNER_ROUTING_TABLE' and 'PERIPHERAL_NODE_TABLE',
			- caches this update in 'UPDATE_DETECT_TABLE' for control flooding,
			- re-broadcasts it if TTL is not equal to zero.
		- If it's an already received iarp_update then, 
			- node discards this update.
	*/
	void recv_IARP_UPDATE(Packet* p); // Receiving IARP Updates
	/*!	\fn void recv_IARP_DATA(Packet* p)
		
		This function is called whenever ZRP receives an IARP_DATA packet. On 
		receiving IARP_DATA, following tasks are performed -
		- If it's destination matches with this node,
			- send it to upper layer.
		- Else
			- forward it to along the route contained in the packet.
	*/
	void recv_IARP_DATA(Packet* p);	// Receiving IARP DATA(Local Traffic)
	/*!	\fn void print_tables()
		
		This function prints Peripheral-Node-List & Inner-Route-List.
	*/
	void print_tables();		// Print all Tables
};





/* [SECTION-4]------------------------------------------------------------------
 * IERP (INTER-ZONE ROUTING PROTOCOL):- SUBSECTIONS CONTAINS FOLLOWING THINGS:
 *	1) DETECTED IERP-REQUEST	(Detected Queries Cache)
 *	2) SENT IERP-REQUEST		(Sent Queries Cache)
 *	3) OUTERROUTE LIST		(Route Table)
 *	4) EXPIRATION-CHECK TIMER	(For Expiration of IERP-Requests)
 *	5) IERP AGENT			(Inter-Zone Routing Agent)
 * -----------------------------------------------------------------------------
 * [SUB-SECTION-4.1]------------------------------------------------------------
 * DETECTED IERP-REQUEST:- DETECTED QUERIES CACHE
 * -----------------------------------------------------------------------------
 */
class DetectedQuery
{
  public:
	// Data...
	nsaddr_t src_;		// 32 bits
	nsaddr_t dest_;		// 32 bits
	int queryID_;		// Query ID
	Time expiry_;		// Expiry of this detected Entry
	PeripheralNodeList pnLst_;	// Query-Coverage Info
	int querySentFlag_;	// Whether the query is Sent/Forwarded or Not
	int totalReplySent_;
	DetectedQuery *next_; 	// Pointer to Next Element

	// Constructors...
	DetectedQuery() : src_(-1), dest_(-1), queryID_(-1), expiry_(-1.0),
		querySentFlag_(0), totalReplySent_(0), next_(NULL) {}
	DetectedQuery(nsaddr_t src, nsaddr_t dest, int queryID, Time expiry,
		PeripheralNodeList *curPNLst) {
	  	src_ = src;
		dest_ = dest;
		queryID_ = queryID;
		expiry_ = expiry;
		querySentFlag_ = FALSE;
		next_ = NULL;
		totalReplySent_ = 0; 
		// Add peripheral node list...
		if(curPNLst == NULL) {	// UNICAST CASE
			// Do Nothing [We wont use coverage info]...
		} else {		// MULTICAST CASE
			// Copies the peripheral-node-list to 'pnLst'
			curPNLst->copyList(&pnLst_);
		}
	}
};

class DetectedQueryList
{
  public:
	// Data...
	DetectedQuery *head_;
	int numQueries_;

	// Constructors...
	DetectedQueryList() : head_(NULL), numQueries_(0) {}
	
	// Methods...
	void addQuery(nsaddr_t src, nsaddr_t dest, int queryID, Time expiry,
		PeripheralNodeList *curPNLst);
	int findQuery(nsaddr_t src, nsaddr_t dest, int queryID, DetectedQuery
		**handle);	// Returns TRUE or FALSE.
	void removeQuery(nsaddr_t src, nsaddr_t dest, int queryID);
	void purgeExpiredQueries();
	void freeList();
};

/* [SUB-SECTION-4.2]------------------------------------------------------------
 * SENT IERP-REQUEST:- SENT QUERIES CACHE
 * -----------------------------------------------------------------------------
 */
class SentQuery
{
  public:
	// Data...
  	nsaddr_t src_;  	// 32 bits
	nsaddr_t dest_;  	// 32 bits
	Time expiry_;		// Expiry of this detected Entry
	SentQuery *next_;	// Pointer to Next Element

	// Constructors...	
	SentQuery() : src_(-1), dest_(-1), expiry_(-1.0), next_(NULL) {}
	SentQuery(nsaddr_t src, nsaddr_t dest, Time expiry) {
		src_ = src;
		dest_ = dest;
		expiry_ = expiry;
		next_ = NULL;
	}
};
class SentQueryList
{
  public:
	// Data...
	SentQuery *head_;
	int numQueries_;

	// Constructors...
	SentQueryList() : head_(NULL), numQueries_(0) {}
	
	// Methods...
	void addQuery(nsaddr_t src, nsaddr_t dest, Time expiry);
	int findQuery(nsaddr_t src, nsaddr_t dest, SentQuery **handle);
	void removeQuery(nsaddr_t src, nsaddr_t dest);
	void purgeExpiredQueries();
	void freeList();
};

/* [SUB-SECTION-4.4]------------------------------------------------------------
 * EXPIRATION-CHECK TIMER:- FOR EXPIRATION OF IERP-REQUESTS
 * -----------------------------------------------------------------------------
 */
 class IERPExpirationTimer : public Handler
{
  public:	
	// Data...
  	ZRPAgent *agent_;	// Pointer to ZRP-Agent
  	Event intr_;
	int expiration_check_period_;
	
	// Constructor...
	IERPExpirationTimer(ZRPAgent* agent) : agent_(agent),
		expiration_check_period_(DEFAULT_EXPIRATION_CHECK_PERIOD) {}

	// Methods...
  	void handle(Event*);  // function handling the event
  	void start(double thistime);
};
 
/* [SUB-SECTION-4.5]------------------------------------------------------------
 * IERP AGENT:- INTER-ZONE ROUTING AGENT
 * -----------------------------------------------------------------------------
 */
class IERPAgent
{
  private:	
	void addMyAddressToRoute(Packet *pold, Packet *pnew);
	void markCoveredPN(DetectedQuery *queryInCache, nsaddr_t lastBC);
  public:
	// Data...
	ZRPAgent *agent_;
	int brpXmitPolicy_;

	// Lists...
	DetectedQueryList dqLst_;
	SentQueryList sqLst_;
	int queryLifeTime_;	// DEFAULT_QUERY_LIFETIME
	int routeLifeTime_;	// DEFAULT_ROUTE_LIFETIME
	int queryRetryTime_;	// DEFAULT_QUERY_RETRY_TIME

	// Timers...
	IERPExpirationTimer ExpirationTimer_; 
	int startup_jitter_;	// For starting the Timers
	
	// Constructors...
	IERPAgent(ZRPAgent *agent) : agent_(agent), 
		brpXmitPolicy_(DEFAULT_BRP_XMIT_POLICY), 
		queryLifeTime_(DEFAULT_QUERY_LIFETIME), 
		routeLifeTime_(DEFAULT_ROUTE_LIFETIME), 	
		queryRetryTime_(DEFAULT_QUERY_RETRY_TIME),
		ExpirationTimer_(agent), 
		startup_jitter_(DEFAULT_STARTUP_JITTER) {}

	// Methods...
	void startUp();
	void recv_IERP_ROUTE_REQUEST_UNI(Packet* p);
	void recv_IERP_ROUTE_REQUEST_MC(Packet* p);
	void recv_IERP_ROUTE_REPLY(Packet* p);
	void recv_IERP_ROUTE_ERROR(Packet* p);
	void recv_IERP_DATA(Packet* p);
	int addLinkStateFromRoute(nsaddr_t *route, int size);
	int removeLinkStateFromBrokenRoute(nsaddr_t lnkSrc, nsaddr_t lnkDest);
	void print_tables();
};





/* [SECTION-5]------------------------------------------------------------------
 * ZRP (ZONE ROUTING PROTOCOL):- SUBSECTIONS CONTAINS FOLLOWING THINGS:
 *	1) ZRP HEADER STRUCTURE
 *	2) PACKET UTILITIES CLASS	(Packet Related Utilities)
 *	3) SEND BUFFER			(Buffer to store un-delivered Upper-layer Packets)
 *	4) ZRP AGENT			(Zone Routing Agent)
 * -----------------------------------------------------------------------------
 * [SUB-SECTION-5.1]------------------------------------------------------------
 * ZRP HEADER STRUCTURE:-
 * -----------------------------------------------------------------------------
 */
class LSU;	// Pre-Declaration
struct hdr_zrp {
	// Common Attributes to All packets... [Default Size = 10 bytes]
  	int zrptype_;     	// Packet type					[1 byte]
  	Time pktsent_;		// Packet Sending time				[Logically not sent]
	int radius_;		// Sender's Zone Radius (Maybe Useful in Future)[Not Used]
	int seq_;     		// Sequence number of a packet			[1 byte]
	nsaddr_t src_;		// 32-bit Address				[4 bytes]
	nsaddr_t dest_;		// 32-bit Address				[4 bytes]

	// IARP Specific...			[IARP size = 2 + 5*links]
	int forwarded_;  	// TRUE if forwarded before			[1 byte]
	LSU *links_;  		// pointer to link state list 			[5 bytes per link - address(4) + link(1)]
    	int numlinks_;		// Number of links in packet			[1 byte]
						
	// IERP Specific...			[IERP size (for REQUEST) = 7 + 4*routelength]
	//					[	(for REPLY) = 3 + 4*routelength]
	//					[	(for ERROR) = 2 + 5(for link info) + 4*routelength] 
	//					[	(for DATA) = 2 + 4*routelength]
	nsaddr_t *mcLst_;	// List of addresses to relay(Multicast) the	[Not Used]
				// route-request query
	int mcLstSize_;		// Size of above list				[Not Used]
	nsaddr_t lastbc_;	// 32-bit Address [Not Used much]		[REQUEST - 4 bytes]
	nsaddr_t *route_; 	// pointer to route list data			[REQUEST/REPLY/ERROR/DATA 4 bytes per address]
	int routelength_;	// Route Length of IERP route stored in route_	[REQUEST/REPLY/ERROR/DATA 1 byte]
  	int routeindex_;  	// Pointer to a current route node in route_	[REQUEST/REPLY/ERROR/DATA 1 byte]
	int queryID_;  		// IERP query id counter			[REQUEST/REPLY 1 byte]

  	// this is where the original data for upper layer pkts
  	// is saved while ZRP routes pkt, at dest this is placed
  	// back into hdrip->dport(), ie this is part of encapsulated data
  	int enc_dport_;
  	int enc_daddr_;
  	packet_t enc_ptype_;

	// Calculate the size of the header
	inline int size() {
		int s=0;
		switch(zrptype_) {
			case NDP_BEACON:
				s = ZRP_DEFAULT_HDR_LEN;
			break;
			case NDP_BEACON_ACK:
				s = ZRP_DEFAULT_HDR_LEN;
			break;
			case IARP_UPDATE:
				s = ZRP_DEFAULT_HDR_LEN + 2 + 5*numlinks_;
			break;
			case IERP_REPLY:
				s = ZRP_DEFAULT_HDR_LEN + 3 + 4*routelength_;
			break;
			case IERP_REQUEST:
				s = ZRP_DEFAULT_HDR_LEN + 7 + 4*routelength_;
			break;
			case IERP_ROUTE_ERROR:
				s = ZRP_DEFAULT_HDR_LEN + 7 + 4*routelength_;
			break;
			case IARP_DATA:
			case IERP_DATA:
				s = ZRP_DEFAULT_HDR_LEN + 2 + 4*routelength_;
			break;
		}
		return s;
	}
  
  	// Packet header access functions
  	static int offset_;
  	inline static int& offset() { return offset_; }
  	inline static hdr_zrp* access(const Packet* p) {
    		return (hdr_zrp*) p->access(offset_);
  	}
};
class LSU
{
  public:	
	nsaddr_t src_;
	nsaddr_t dest_;
	int isUp_;

	// Constructors...
	LSU() : src_(-1), dest_(-1), isUp_(LINKDOWN) {}	// Invalid Entries...
	LSU(nsaddr_t src, nsaddr_t dest, int isUp) : src_(src), dest_(dest),
	    isUp_(isUp) {}
};

 /* [SUB-SECTION-5.2]------------------------------------------------------------
 * PACKET UTILITIES CLASS:- PACKET RELATED UTILITIES
 * -----------------------------------------------------------------------------
 */
class PacketUtil
{
  public:
	// Data...
	int seq_;
	ZRPAgent *agent_;
	int startup_jitter_;

	PacketUtil(ZRPAgent *agent) : seq_(0), agent_(agent),
		startup_jitter_(DEFAULT_STARTUP_JITTER) {}

	// Methods...
	Packet* pkt_create(ZRPTYPE zrp_type, nsaddr_t addressee, int ttl); 
  	void pkt_copy(Packet *pfrom, Packet *pto);
	void inc_seq() { seq_++; if (seq_ > MAX_SEQUENCE_ID) seq_ = 1;}
	void pkt_send(Packet *p, nsaddr_t addressee, Time delay);
	void pkt_broadcast(Packet *p, Time randomJitter);
	void pkt_add_LSU_space(Packet *p, int size);
	void pkt_free_LSU_space(Packet *p);
	void pkt_add_ROUTE_space(Packet *p, int size);
	void pkt_free_ROUTE_space(Packet *p);
	void pkt_add_ADDRESS_space(Packet *p, int size);
	void pkt_free_ADDRESS_space(Packet *p);
	int pkt_AmIMultiCastReciver(Packet *p, nsaddr_t addressToCheck);
	int pkt_AmIOnTheRoute(Packet *p, nsaddr_t addressToCheck);
	void pkt_print_links(Packet *p);
	void pkt_print_route(Packet *p);
	void pkt_drop(Packet *p);
};
 
/* [SUB-SECTION-5.3]------------------------------------------------------------
 * SEND BUFFER:- BUFFER TO STORE UN-DELIVERED UPPER-LAYER PACKETS
 * -----------------------------------------------------------------------------
 */
class SendBufferEntry
{
  public:
	Packet *pkt_;		// Packet to Send from Upper-Layer
	nsaddr_t dest_; 	// Address of Destination
	Time expiry_;		// Time to expire the packet
	SendBufferEntry *next_;	// Link to next entry in the list

	SendBufferEntry() : pkt_(NULL), dest_(-1), expiry_(0.00), next_(NULL) {}
	SendBufferEntry(Packet *pkt, nsaddr_t dest, Time expiry) : pkt_(pkt), 
		dest_(dest), expiry_(expiry), next_(NULL) {}
};

class SendBuffer
{
  public:
	ZRPAgent *agent_;
	SendBufferEntry *head_;
	int numPackets_;

	SendBuffer(ZRPAgent *agent) : agent_(agent), head_(NULL), numPackets_(0) {}

	// Methods...
	void addPacket(Packet *pkt, nsaddr_t dest, Time expiry);
	void purgeExpiredPackets();
	void freeList();
};


/* [SUB-SECTION-5.4]------------------------------------------------------------
 * ZRP AGENT:- ZONE ROUTING AGENT
 * -----------------------------------------------------------------------------
 */
class ZRPAgent : public Agent {
 public:
	// Tcl Related...
	char*  myid_;   	// (char *)myid_ is string equiv of (int)myaddr_
	PriQueue* ll_queue;
	Trace* tracetarget;
	MobileNode* node_;
	NsObject* port_dmux_;

	// Common Data...
	nsaddr_t myaddr_;	// My-own-address
	int radius_;   		// Zone-Radius
	int tx_; 		// total pkts transmitted by agent
	int rx_; 		// total pkts received by agent
	int queryID_;		// IERP query IDs

	// General Objects...
	SendBuffer sendBuf_;
	PacketUtil pktUtil_;
	NDPAgent ndpAgt_;
	IARPAgent iarpAgt_;
	IERPAgent ierpAgt_;

	// Constructors...
	ZRPAgent(); 		// Default Constructor - (1)
	ZRPAgent(nsaddr_t id);	// Parameterized Constructor - (2)

	// Methods...
		// 1. General...
		void startUp();
		int  command (int argc, const char*const* argv);
		void recv(Packet * p, Handler *);
		void route_pkt(Packet* p, nsaddr_t dest);
		void route_SendBuffer_pkt();
		void sendPacketUsingIARPRoute(Packet *p, nsaddr_t dest, Time delay);
		int  initialized() { return 1 && target_; }
		void print_tables();

		// 2. Mac Failed...
		void mac_failed(Packet *p);
	
		// 3. Methods for Packet Handling
		Packet* Myallocpkt() {
			Packet *p = allocpkt(); 	// fresh new packet
			return (p);
		}
		void Mydrop(Packet *p, const char *s) {
			 drop(p, s);
		}
		void XmitPacket(Packet *p, Time randomJitter) {
			Scheduler & s = Scheduler::instance();
		  	s.schedule(target_, p, randomJitter);
		}
};

#endif
